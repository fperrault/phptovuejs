var app = new Vue({

    el: '.container',

    data: {
        taches: [],
        //isShare: navigator.share?true:false
    },

    created: function () {
        console.log("Démarrage TODO-APP")
    },

    beforeMount: function(){
        this.recupererListe()
    },
    
    methods: {
        ajout: function(){
            var contenu = document.getElementById("texte")
            if(contenu.value == ""){
                swal("Aie, aie, aie...", "Il faut définir ce qui est à faire ;)", "error")
            } else {
                var form = new FormData()
                form.append('texte', contenu.value)
                fetch("api/creation", {
                    method: "POST",
                    body: form,
                    credentials: 'same-origin'
                })
                .then(function(response){
                    return response.json()
                })
                .then(function(response){
                    if(response.success){
                        app.recupererListe()
                        contenu.value = ""
                        swal("Bien joué!", "La tâche est bien enregistrée", "success")
                    }else{
                        swal("Aie, aie, aie...", "Nous avons rencontrer un problème...", "error")
                    }
                })
                .catch(function(error){
                    console.log('Récupération impossible: ' + error.message)
                })
            }
        },

        recupererListe: function(){
            fetch('api/liste.php', {
                method: "GET",
                credentials: 'same-origin'
            })
            .then(function(response){
              return response.json()
            })
            .then(function(response) {
                app.taches = response
            })
            .catch(function(error) {
              console.log('Récupération impossible: ' + error.message)
            })
        },

        terminer: function(id){
            var form = new FormData()
            form.append('id', id)
            fetch("api/terminer.php", {
                method: "POST",
                body: form,
                credentials: 'same-origin'
            })
            .then(function(response){
                return response.json()
            })
            .then(function(response){
                if(response.success){
                    app.recupererListe()
                    swal("Terminé!", "La tâche a bien été complétée", "success")
                }else{
                    swal("Aie, aie, aie...", "Nous avons rencontrer un problème...", "error")
                }
            })
            .catch(function(error){
                console.log('Récupération impossible: ' + error)
                swal("Aie, aie, aie...", "Nous avons rencontrer un problème...", "error")
            })
        },

        supprimer: function(id){
            var form = new FormData()
            form.append('id', id)
            fetch("api/suppression.php", {
                method: "POST",
                body: form,
                credentials: 'same-origin'
            })
            .then(function(response){
                return response.json()
            })
            .then(function(response){
                if(response.success){
                    app.recupererListe()
                    swal("Supprimé!", "La tâche a bien été supprimée", "success")
                }else{
                    swal("Aie, aie, aie...", "Nous avons rencontrer un problème...", "error")
                }
            })
            .catch(function(error){
                console.log('Récupération impossible: ' + error)
                swal("Aie, aie, aie...", "Nous avons rencontrer un problème...", "error")
            })
        },

        /*
        share: function(){
            navigator.share({
                title: 'VueJS-Todo',
                text: texte,
                url: ""
            })
            .then(function(){})
            .catch(function(){})
        }*/
    }
})
