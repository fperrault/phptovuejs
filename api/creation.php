<?php
    include("fonctions.php");

    $texte = $_POST["texte"];
    $verif = false;

    // Dans le cadre du traitement, vérifier si $_SESSION["todos"] est bien un tableau (is_array…)
    if(isset($_SESSION["todos"]) && !is_array($_SESSION["todos"])){
        $_SESSION["todos"] = array();
    }


    // Sauvegarder dans la Session.
    if(isset($texte) && !empty($texte)){
        $todo = array("id" => uniqid(), "texte" => $_POST["texte"], "date" => time(), "termine" => false);
        $_SESSION["todos"][$todo["id"]] = $todo;

        $verif = true;
    }

    // Afficher un JSON
    retour($verif);

?>