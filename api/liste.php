<?php

  include("fonctions.php");
  
  header("content-type: application/json");
  // Vérifier si en $_SESSION["todos"] est bien un tableau
  if(isset($_SESSION["todos"]) && is_array($_SESSION["todos"])){
    echo json_encode($_SESSION["todos"]);
  }else{
    echo json_encode(array());
  }
?>