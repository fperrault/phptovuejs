<?php
    include("fonctions.php");

    $id = $_POST["id"];
    $verif = false;

    if(isset($id) && !empty($id)){
        
        if(array_key_exists($id, $_SESSION["todos"])){
            $_SESSION["todos"][$id]["termine"] = true;
            $verif = true;
        }
    }

    retour($verif);
    
?>